package business;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative()
public class OrdersBusinessService implements OrdersBusinessInterface {

	//Class Scoped Properties
	List<Order> orders;
	
    /**
     * Default constructor. 
     */
	public OrdersBusinessService() {
		// Implement dummy order list
		// Array of dummy orders.
		Order[] dummyOrders = new Order[] { new Order("E1234", "AlphaTech TV", 139.99f, 1),
				new Order("LT321", "Acme Catapult", 49.99f, 1), new Order("BEV01", "Diet Xola", 7.49f, 5),
				new Order("U5982", "Poxie Paper Plates", 4.99f, 10) };

		// Initialize orders with a dummyOrder array
		orders = Stream.of(dummyOrders).collect(Collectors.toList());
	}

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        System.out.println("Hello from the OrdersBusinessService");
    }

    /**
     * @return List<Order> orders
     */
	public List<Order> getOrders() {
		return this.orders;
	}

	/**
	 * @param List<Order> orders to set List<Order> orders
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
		
	}

}
